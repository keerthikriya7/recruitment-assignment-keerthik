import { CONSTS } from './../../../constants';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hotel-home',
  templateUrl: './hotel-home.component.html',
  styleUrls: ['./hotel-home.component.css']
})
export class HotelHomeComponent implements OnInit {

  public menu: any;
  public Tables: any;
  public Managers: any;
  public Chefs: any;
  public selectedMenu: any;
  public selectedTable: any;
  public selectedManager: any;
  public selectedChef: any;
  public show = false as boolean;
  public selectedArr = [] as any[];

  constructor() { }

  ngOnInit() {
    this.menu = CONSTS.Menu.items;
    this.Tables = CONSTS.Tables.items;
    this.Managers = CONSTS.Managers.items;
    this.Chefs = CONSTS.Chefs.items;
  }

  public menuSelect(menuSelect) {
  }

  public tableSelect() {
  }

  public selectedManagerVal() {
  }

  public selectedChefsVal() {
  }

  public selectedValues() {
    alert('order Placed Successfully...!');
    this.show = true;
    this.selectedArr.push({'menu': this.selectedMenu, 'Tables': this.selectedTable, 'Managers': this.selectedManager,
     'Chefs': this.selectedChef});
  }
  public selected() {

  }
  public cancel() {

  }
}
