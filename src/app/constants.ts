const APPLN_CONST = {
  Menu:
      { 'items' : [
          {
              'name': 'Masala Papad',
              'section': 'Starters',
              'price': 30
          },
          {
              'name': 'Chilly Panner Manchurian',
              'section': 'Starters',
              'price': 250
          },
          {
              'name': 'Veg Biryani',
              'section': 'Main course',
              'price': 300
          },
          {
              'name': 'Panner spl Biryani',
              'section': 'Main course',
              'price': 350
          },
          {
              'name': 'Ice-cream',
              'section': 'Desserts',
              'price': 50
          },
          {
              'name': 'Chocolate Brownie',
              'section': 'Desserts',
              'price': 120
          }
      ]
    }
  ,
  Tables :
    {
      'items': [
          {
              'Tableid': 1,
              'capacity': 4,
              'waiter': 'Madhav'
          },
          {
              'Tableid' : 2,
              'capacity': 6,
              'waiter': 'Keshav'
          },
          {
              'Tableid': 3,
              'capacity': 2,
              'waiter': 'Paresh'
          },
          {
              'Tableid': 4,
              'capacity': 8,
              'waiter': 'Kishore'
          },
          {
              'Tableid': 5,
              'capacity': 4,
              'waiter': 'Naresh'
          },
          {
              'Tableid': 6,
              'capacity': 4,
              'waiter': 'Mutthu'
          }
      ]
    }
  ,
  Managers :
    {
      'items': [
          {
            'name': 'Abhishek Kumar',
            'areaofservice': 'getOrder'
          },
          {
            'name': 'Venkat Swami ',
            'areaofservice': 'receiveComplaint'
          },
          {
            'name': 'Suji John',
            'areaofservice': 'foodPrep'
          }
      ]
    }
  ,
  Chefs :
    {
      'items': [
          {
              'name': 'Amith',
              'areaofexpertise': 'starters'
          },
          {
              'name': 'Bhanu',
              'areaofexpertise': 'maincourse',
          },
          {
              'name': 'Giridhar',
              'areaofexpertise': 'desserts'
          }
      ]
    }

};

export const CONSTS = Object.freeze(APPLN_CONST);
